FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

ENTRYPOINT ["java", "-Dserver.port=9090", "-Dapplication.home=.", "-Dproject.name=developer-game", "-Dspring.profiles.active=prod", "-Dapplication.environment=prod", "-cp", "app:app/lib/*", "com.mxninja.workSelectionGame.MainKt"]