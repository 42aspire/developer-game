package com.mxninja.workSelectionGame.adapters.entities

import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author Mohammad Ali
 * date: 2018-11-30 14:17.
 */

@Entity
@Table(name = "User")
class UserEntity {

    @Id
    lateinit var id: String
    lateinit var firstName: String
    lateinit var lastName: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var birthday: LocalDate
    lateinit var profession: String
    var yearsExperience: Int? = null
    var profileLinkedIn: String? = null
    var profileFacebook: String? = null
    var profileStackOverFlow: String? = null

    /*internal info*/
    lateinit var createdAt: LocalDateTime
    lateinit var enterPassword: String
    var pass: Boolean = false

}