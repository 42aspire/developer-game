package com.mxninja.workSelectionGame.adapters.entities

import com.mxninja.workSelectionGame.aggregates.UserAnswerId
import com.mxninja.workSelectionGame.aggregates.UserId
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:57.
 */

@Entity
@Table(name = "UserAnswer")
class UserAnswerEntity {

    @Id
    lateinit var id: String
    lateinit var userId: String
    var questionId: Int = 0
    lateinit var answer: String
    lateinit var comment: String
    lateinit var createdAt: LocalDateTime

}