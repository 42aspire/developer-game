package com.mxninja.workSelectionGame.adapters.entities

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "Questions")
class QuestionsEntity {

    @Id
    var id: Int = 0
    lateinit var question: String
    lateinit var message: String
    var nextQuestion: Int = 0

}