package com.mxninja.workSelectionGame.adapters.repositories.users

import com.mxninja.workSelectionGame.adapters.entities.UserEntity
import com.mxninja.workSelectionGame.aggregates.UserAggregate
import java.util.*

class UserRepositoryJpaImpl(private val userDAO: UserDAO) : UserRepository {

    override fun save(userAggregate: UserAggregate): UserEntity {
        return userDAO.save(convertToEntity(userAggregate))
    }

    override fun findByEmail(email: String): Optional<UserEntity> {
        return userDAO.findByEmail(email.toLowerCase())
    }

    private fun convertToEntity(userAggregate: UserAggregate): UserEntity {
        val userEntity = UserEntity()
        userEntity.id = userAggregate.id.toString()
        userEntity.firstName = userAggregate.firstName
        userEntity.lastName = userAggregate.lastName
        userEntity.email = userAggregate.email
        userEntity.phone = userAggregate.phone
        userEntity.birthday = userAggregate.birthday
        userEntity.profession = userAggregate.profession
        userEntity.yearsExperience = userAggregate.yearsExperience
        userEntity.profileLinkedIn = userAggregate.profileLinkedIn
        userEntity.profileFacebook = userAggregate.profileFacebook
        userEntity.profileStackOverFlow = userAggregate.profileStackOverFlow
        userEntity.createdAt = userAggregate.createdAt
        userEntity.pass = userAggregate.pass
        userEntity.enterPassword = userAggregate.enterPassword
        return userEntity
    }
}