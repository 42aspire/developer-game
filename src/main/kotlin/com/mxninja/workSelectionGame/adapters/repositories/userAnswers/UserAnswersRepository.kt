package com.mxninja.workSelectionGame.adapters.repositories.userAnswers

import com.mxninja.workSelectionGame.adapters.entities.UserAnswerEntity
import com.mxninja.workSelectionGame.aggregates.UserAnswerAggregate

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:50.
 */
interface UserAnswersRepository {

    fun save(userAnswerAggregate: UserAnswerAggregate): UserAnswerEntity


}