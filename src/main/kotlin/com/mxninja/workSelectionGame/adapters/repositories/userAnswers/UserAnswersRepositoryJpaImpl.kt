package com.mxninja.workSelectionGame.adapters.repositories.userAnswers

import com.mxninja.workSelectionGame.adapters.entities.UserAnswerEntity
import com.mxninja.workSelectionGame.aggregates.UserAnswerAggregate

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:58.
 */
class UserAnswersRepositoryJpaImpl(private val userAnswersDAO: UserAnswersDAO) : UserAnswersRepository {

    override fun save(userAnswerAggregate: UserAnswerAggregate): UserAnswerEntity {
        return userAnswersDAO.save(convertToEntity(userAnswerAggregate))
    }

    private fun convertToEntity(userAnswerAggregate: UserAnswerAggregate): UserAnswerEntity {
        val userAnswerEntity = UserAnswerEntity()
        userAnswerEntity.id = userAnswerAggregate.id.toString()
        userAnswerEntity.userId = userAnswerAggregate.userId.toString()
        userAnswerEntity.questionId = userAnswerAggregate.questionId
        userAnswerEntity.answer = userAnswerAggregate.answer
        userAnswerEntity.comment = userAnswerAggregate.comment
        userAnswerEntity.createdAt = userAnswerAggregate.createdAt
        return userAnswerEntity
    }
}