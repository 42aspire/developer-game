package com.mxninja.workSelectionGame.adapters.repositories.questions

import com.mxninja.workSelectionGame.adapters.entities.QuestionsEntity
import com.mxninja.workSelectionGame.aggregates.QuestionAggregate
import java.util.*

/**
 * @author Mohammad Ali
 * date: 2018-12-01 14:29.
 */
class QuestionsRepositoryJpaImpl(private val questionsDAO: QuestionsDAO) : QuestionsRepository {

    override fun save(questionAggregate: QuestionAggregate): QuestionsEntity {
        return questionsDAO.save(convertToEntity(questionAggregate))
    }

    override fun findById(id: Int): Optional<QuestionsEntity> {
        return questionsDAO.findById(id)
    }

    override fun deleteAll() {
        questionsDAO.deleteAll()
    }

    private fun convertToEntity(questionAggregate: QuestionAggregate): QuestionsEntity {
        val questionsEntity = QuestionsEntity()
        questionsEntity.id = questionAggregate.id
        questionsEntity.message = questionAggregate.message
        questionsEntity.question = questionAggregate.question
        questionsEntity.nextQuestion = questionAggregate.nextQuestion
        return questionsEntity
    }
}