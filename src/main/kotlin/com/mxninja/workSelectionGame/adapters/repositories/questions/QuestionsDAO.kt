package com.mxninja.workSelectionGame.adapters.repositories.questions

import com.mxninja.workSelectionGame.adapters.entities.QuestionsEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * @author Mohammad Ali
 * date: 2018-12-01 14:27.
 */
@Repository
interface QuestionsDAO : JpaRepository<QuestionsEntity, Int>