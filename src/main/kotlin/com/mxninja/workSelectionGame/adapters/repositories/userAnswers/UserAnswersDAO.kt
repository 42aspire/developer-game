package com.mxninja.workSelectionGame.adapters.repositories.userAnswers

import com.mxninja.workSelectionGame.adapters.entities.UserAnswerEntity
import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:57.
 */
interface UserAnswersDAO : JpaRepository<UserAnswerEntity, String>