package com.mxninja.workSelectionGame.adapters.repositories.users

import com.mxninja.workSelectionGame.adapters.entities.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserDAO : JpaRepository<UserEntity, String> {

    fun findByEmail(email: String): Optional<UserEntity>

}