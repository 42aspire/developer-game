package com.mxninja.workSelectionGame.adapters.repositories.users

import com.mxninja.workSelectionGame.adapters.entities.UserEntity
import com.mxninja.workSelectionGame.aggregates.UserAggregate
import java.util.*

interface UserRepository {

    fun save(userAggregate: UserAggregate): UserEntity

    fun findByEmail(email: String): Optional<UserEntity>

}