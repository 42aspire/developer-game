package com.mxninja.workSelectionGame.adapters.repositories.questions

import com.mxninja.workSelectionGame.adapters.entities.QuestionsEntity
import com.mxninja.workSelectionGame.aggregates.QuestionAggregate
import java.util.*

/**
 * @author Mohammad Ali
 * date: 2018-12-01 14:28.
 */
interface QuestionsRepository {

    fun save(questionAggregate: QuestionAggregate): QuestionsEntity

    fun findById(id: Int): Optional<QuestionsEntity>

    fun deleteAll()

}