package com.mxninja.workSelectionGame.adapters.controllers

import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsRepository
import com.mxninja.workSelectionGame.adapters.repositories.users.UserRepository
import com.mxninja.workSelectionGame.aggregates.UserAggregate
import com.mxninja.workSelectionGame.aggregates.UserId
import com.mxninja.workSelectionGame.commands.RegistrationCommand
import com.mxninja.workSelectionGame.domains.QuestionDomain
import com.mxninja.workSelectionGame.tools.encrypt
import com.mxninja.workSelectionGame.tools.getJwt
import com.mxninja.workSelectionGame.tools.getRandomString
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * @author Mohammad Ali
 * date: 2018-12-01 12:56.
 */

@RestController
@RequestMapping("registration")
class RegistrationController(
        private val userRepository: UserRepository,
        private val questionsRepository: QuestionsRepository
) {

    @PostMapping
    @ResponseBody
    fun registration(@RequestBody command: RegistrationCommand): QuestionDomain {

        val user = userRepository.findByEmail(command.email.toLowerCase())
        if (user.isPresent) {
            return QuestionDomain(
                    "this email was registered before, please use another email",
                    "",
                    "",
                    ""
            )
        }

        val userAggregate = UserAggregate()
        userAggregate.id = UserId.new()
        userAggregate.firstName = command.firstName.toLowerCase()
        userAggregate.lastName = command.lastName.toLowerCase()
        userAggregate.email = command.email.toLowerCase()
        userAggregate.phone = command.phone
        userAggregate.birthday = LocalDate.parse(command.birthday, DateTimeFormatter.ofPattern("yyyyMMdd"))
        userAggregate.profession = command.profession
        userAggregate.yearsExperience = command.yearsExperience
        userAggregate.profileLinkedIn = command.profileLinkedIn
        userAggregate.profileFacebook = command.profileFacebook
        userAggregate.profileStackOverFlow = command.profileStackOverFlow

        userAggregate.createdAt = LocalDateTime.now()
        userAggregate.enterPassword = getRandomString(8)

        val userEntity = userRepository.save(userAggregate)

        val token = getJwt(userEntity.email, userEntity.enterPassword, ZonedDateTime.now().plusDays(1))

        val questionsEntity = questionsRepository.findById(1).get()

        val optionNextQuestion = questionsEntity.id.toString().encrypt()


        return QuestionDomain(
                String.format(questionsEntity.message, token),
                questionsEntity.question,
                "",
                optionNextQuestion.get()
        )
    }

}