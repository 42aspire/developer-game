package com.mxninja.workSelectionGame.adapters.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView

/**
 * @author Mohammad Ali
 * date: 2018-12-01 17:18.
 */

@Controller
class WelcomeController {

    @GetMapping("/")
    @ResponseBody
    fun welcome(): ModelAndView {
        val modelAndView = ModelAndView()
        modelAndView.viewName = "index.html"
        return modelAndView
    }
}