package com.mxninja.workSelectionGame.adapters.controllers

import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsRepository
import com.mxninja.workSelectionGame.adapters.repositories.userAnswers.UserAnswersRepository
import com.mxninja.workSelectionGame.adapters.repositories.users.UserRepository
import com.mxninja.workSelectionGame.aggregates.JwtAggregate
import com.mxninja.workSelectionGame.aggregates.UserAnswerAggregate
import com.mxninja.workSelectionGame.aggregates.UserAnswerId
import com.mxninja.workSelectionGame.aggregates.UserId
import com.mxninja.workSelectionGame.commands.AnswerCommand
import com.mxninja.workSelectionGame.domains.QuestionDomain
import com.mxninja.workSelectionGame.tools.decrypt
import com.mxninja.workSelectionGame.tools.encrypt
import com.mxninja.workSelectionGame.tools.parseJwtToken
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:38.
 */

@RestController
@RequestMapping("answer")
class AnswerController(
        private val userRepository: UserRepository,
        private val questionsRepository: QuestionsRepository,
        private val userAnswersRepository: UserAnswersRepository
) {

    @PostMapping
    fun answer(@RequestBody command: AnswerCommand): QuestionDomain {
        val questionId = command.questionToken.decrypt().get().toInt()
        val jwtUser = command.yourToken.parseJwtToken()
        val currentQuestion = questionsRepository.findById(questionId).get()
        val currentUser = userRepository.findByEmail(jwtUser!!.email).get()
        val flag = checkTheAnswer(questionId, command, jwtUser)

        if (flag) {
            val userAnswerAggregate = UserAnswerAggregate()
            userAnswerAggregate.id = UserAnswerId.newUserAnswerId()
            userAnswerAggregate.userId = UserId.of(currentUser.id)
            userAnswerAggregate.answer = command.answer
            userAnswerAggregate.comment = command.comment
            userAnswerAggregate.questionId = questionId
            userAnswerAggregate.createdAt = LocalDateTime.now()
            userAnswersRepository.save(userAnswerAggregate)

            val nextQuestion = questionsRepository.findById(currentQuestion.nextQuestion).get()
            return QuestionDomain(
                    nextQuestion.message,
                    nextQuestion.question,
                    "",
                    nextQuestion.id.toString().encrypt().get()
            )
        } else {
            return QuestionDomain(
                    "wrong answer",
                    "",
                    "",
                    ""
            )
        }
    }

    private fun checkTheAnswer(questionId: Int, command: AnswerCommand, jwtUser: JwtAggregate): Boolean = when (questionId) {
        1 -> checkAnswerPasswordInToken(command.answer, jwtUser.password)
        2 -> checkAnswerExpDate(command.answer, jwtUser.expDate)
        else -> false
    }

    private fun checkAnswerPasswordInToken(password: String, jwtPassword: String): Boolean = jwtPassword == password

    private fun checkAnswerExpDate(answer: String, timeInMills: Long): Boolean {
        val cal = Calendar.getInstance()
        cal.timeInMillis = timeInMills
        val simplerDateFormatter = SimpleDateFormat("yyyyMMddHHmm")
        val expDate = simplerDateFormatter.format(cal.time)
        return expDate == answer
    }

}