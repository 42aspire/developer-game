package com.mxninja.workSelectionGame.configuration

import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsDAO
import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsRepository
import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsRepositoryJpaImpl
import com.mxninja.workSelectionGame.adapters.repositories.userAnswers.UserAnswersDAO
import com.mxninja.workSelectionGame.adapters.repositories.userAnswers.UserAnswersRepository
import com.mxninja.workSelectionGame.adapters.repositories.userAnswers.UserAnswersRepositoryJpaImpl
import com.mxninja.workSelectionGame.adapters.repositories.users.UserDAO
import com.mxninja.workSelectionGame.adapters.repositories.users.UserRepository
import com.mxninja.workSelectionGame.adapters.repositories.users.UserRepositoryJpaImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class AppConfiguration(
        private val questionsDAO: QuestionsDAO,
        private val userDAO: UserDAO,
        private val userAnswersDAO: UserAnswersDAO) {

    @Bean
    fun getQuestionsRepository(): QuestionsRepository = QuestionsRepositoryJpaImpl(questionsDAO)

    @Bean
    fun getUserRepository(): UserRepository = UserRepositoryJpaImpl(userDAO)

    @Bean
    fun getUserAnswerRepository(): UserAnswersRepository = UserAnswersRepositoryJpaImpl(userAnswersDAO)

}