package com.mxninja.workSelectionGame.aggregates

import java.time.LocalDate
import java.time.LocalDateTime

/**
 * @author Mohammad Ali
 * date: 2018-11-30 14:23.
 */
class UserAggregate {

    lateinit var id: UserId
    lateinit var firstName: String
    lateinit var lastName: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var birthday: LocalDate
    lateinit var profession: String
    var yearsExperience: Int? = null
    var profileLinkedIn: String? = null
    var profileFacebook: String? = null
    var profileStackOverFlow: String? = null

    /*internal info*/
    lateinit var createdAt: LocalDateTime
    lateinit var enterPassword: String
    var pass: Boolean = false

}