package com.mxninja.workSelectionGame.aggregates

import java.util.*

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:53.
 */
class UserAnswerId private constructor(uuid: UUID) {

    companion object {
        fun newUserAnswerId(): UserAnswerId = UserAnswerId(UUID.randomUUID())
        fun of(id: String): UserAnswerId = UserAnswerId(UUID.fromString(id))
    }

    var uuid: UUID
        private set

    init {
        this.uuid = uuid
    }

    override fun toString(): String {
        return uuid.toString()
    }

}

