package com.mxninja.workSelectionGame.aggregates

class QuestionAggregate {

    var id: Int = 0
    lateinit var question: String
    lateinit var message: String
    var nextQuestion: Int = 0
}