package com.mxninja.workSelectionGame.aggregates

import io.jsonwebtoken.impl.DefaultClaims

/**
 * @author Mohammad Ali
 * date: 2018-12-01 13:15.
 */
class JwtAggregate(default: DefaultClaims) {

    val email = default["email"].toString()
    val password = default["password"].toString()
    val expDate = default["exp"].toString().toLong()

}