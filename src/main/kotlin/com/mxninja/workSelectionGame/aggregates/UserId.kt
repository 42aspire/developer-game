package com.mxninja.workSelectionGame.aggregates

import java.util.*

/**
 * @author Mohammad Ali
 * date: 2018-11-30 14:20.
 */
class UserId private constructor(uuid: UUID) {

    companion object {
        fun new(): UserId = UserId(UUID.randomUUID())
        fun of(id: String): UserId = UserId(UUID.fromString(id))
    }

    var uuid: UUID
        private set

    init {
        this.uuid = uuid
    }

    override fun toString(): String {
        return uuid.toString()
    }

}