package com.mxninja.workSelectionGame.aggregates

import java.time.LocalDateTime

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:50.
 */
class UserAnswerAggregate {

    lateinit var id: UserAnswerId
    lateinit var userId: UserId
    var questionId: Int = 0
    lateinit var answer: String
    lateinit var comment: String
    lateinit var createdAt: LocalDateTime

}