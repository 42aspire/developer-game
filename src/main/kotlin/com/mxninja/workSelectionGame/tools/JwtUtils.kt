package com.mxninja.workSelectionGame.tools

import com.mxninja.workSelectionGame.aggregates.JwtAggregate
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.time.ZonedDateTime
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.LinkedHashMap
import io.jsonwebtoken.impl.DefaultClaims
import io.jsonwebtoken.impl.DefaultJws



/**
 * @author Mohammad Ali
 * date: 2018-12-01 13:58.
 */


private val key by lazy {
    val keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES")
    keyFactory.generateSecret(PBEKeySpec("uZTKGHTocYpVMRRwimepOtqnP2V1PAB5".toCharArray()))
}

fun getJwt(email: String, password: String, exp: ZonedDateTime): String {
    val claims = LinkedHashMap<String, Any>()
    claims["email"] = email
    claims["password"] = password
    claims["exp"] = exp.toInstant().toEpochMilli()
    return Jwts.builder()
            .setClaims(claims as Map<String, Any>)
            .signWith(SignatureAlgorithm.HS512, key)
            .compact()
}

fun getRandomString(length: Int): String {
    val chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray()
    val sb = StringBuilder()
    val random = Random()
    for (i in 0 until length) {
        val c = chars[random.nextInt(chars.size)]
        sb.append(c)
    }
    return sb.toString()
}

private const val KEY = "6Ld9qT6D=lH4;h+W"
private const val INIT_VECTOR = "o1AY2u4BOBTuaTmA"
private const val CIPHER = "AES/CBC/PKCS5PADDING"

fun String.encrypt(): Optional<String> {
    return try {
        val iv = IvParameterSpec(INIT_VECTOR.toByteArray(charset("UTF-8")))
        val skeySpec = SecretKeySpec(KEY.toByteArray(charset("UTF-8")), "AES")
        val cipher = Cipher.getInstance(CIPHER)
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv)

        val encrypted = cipher.doFinal(this.toByteArray())
        Optional.of(Base64.getEncoder().encodeToString(encrypted))
    } catch (ex: Exception) {
        Optional.empty()
    }
}

fun String.decrypt(): Optional<String> {
    return try {
        val iv = IvParameterSpec(INIT_VECTOR.toByteArray(charset("UTF-8")))
        val skeySpec = SecretKeySpec(KEY.toByteArray(charset("UTF-8")), "AES")

        val cipher = Cipher.getInstance(CIPHER)
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv)

        val original = cipher.doFinal(Base64.getDecoder().decode(this))

        Optional.of(String(original))
    } catch (ex: Exception) {
        Optional.empty()
    }
}

fun String.parseJwtToken(): JwtAggregate? {
    return try {
        val defaultJwt = Jwts.parser().setSigningKey(key).parseClaimsJws(this) as DefaultJws<*>
        val defaultClaims = defaultJwt.body as DefaultClaims
        JwtAggregate(defaultClaims)
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}