package com.mxninja.workSelectionGame.domains

/**
 * @author Mohammad Ali
 * date: 2018-11-30 16:57.
 */
data class QuestionDomain(
        val message: String,
        val question: String,
        val hint: String,
        val questionToken: String
)