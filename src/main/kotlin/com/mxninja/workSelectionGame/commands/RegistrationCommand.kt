package com.mxninja.workSelectionGame.commands

/**
 * @author Mohammad Ali
 * date: 2018-12-01 12:57.
 */
data class RegistrationCommand(
        var firstName: String,
        var lastName: String,
        var email: String,
        var phone: String,
        var birthday: String,
        var profession: String,
        var yearsExperience: Int,
        var profileLinkedIn: String?,
        var profileFacebook: String?,
        var profileStackOverFlow: String?
)