package com.mxninja.workSelectionGame.commands

/**
 * @author Mohammad Ali
 * date: 2018-12-01 15:37.
 */
data class AnswerCommand(
        val answer: String,
        val yourToken: String,
        val comment: String,
        val questionToken: String
)