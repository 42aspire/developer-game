package com.mxninja.workSelectionGame.loaders

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.mxninja.workSelectionGame.adapters.repositories.questions.QuestionsRepository
import com.mxninja.workSelectionGame.aggregates.QuestionAggregate
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import org.springframework.util.ResourceUtils
import java.io.FileInputStream


/**
 * @author Mohammad Ali
 * date: 2018-12-01 14:39.
 */

@Component
class QuestionsLoader(private val questionsRepository: QuestionsRepository) : CommandLineRunner {

    override fun run(vararg args: String?) {
        val file = ResourceUtils.getFile("classpath:static/questions.json")
        val fileInputStream = FileInputStream(file)
        val objectMapper = ObjectMapper()
        val listOfQuestions = objectMapper.readValue(fileInputStream, object : TypeReference<List<QuestionAggregate>>() {
        }) as List<QuestionAggregate>
        questionsRepository.deleteAll()
        listOfQuestions.stream().forEach {
            questionsRepository.save(it)
        }

    }
}