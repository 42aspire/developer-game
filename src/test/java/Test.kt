import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Mohammad Ali
 * date: 2018-12-01 13:56.
 */

fun main(args: Array<String>) {

    val cal = Calendar.getInstance()
    cal.timeInMillis = 1543760769574
    val simplerDateFormater = SimpleDateFormat("yyyyMMddHHmm")
    println(simplerDateFormater.format(cal.time))

}